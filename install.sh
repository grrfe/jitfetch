#!/usr/bin/env bash
tag=$(git describe --tags)
/usr/bin/python -E -m pip install "jitfetch @ git+https://gitlab.com/grrfe/jitfetch@${tag}"
