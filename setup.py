import setuptools

with open("requirements.txt") as f:
    requirements = f.readlines()

setuptools.setup(
    name="jitfetch",
    packages=setuptools.find_packages(),
    description="Fetch JitPack packages on the command line",
    author="grrfe",
    author_email="grrfe@420blaze.it",
    setuptools_git_versioning={"enabled": True},
    setup_requires=["setuptools-git-versioning<2"],
    url="https://gitlab.com/grrfe/jitfetch",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    install_requires=requirements,
    python_requires=">=3.7",
    entry_points={
        "console_scripts": [
            'jitfetch = core.cli:parse_input',
        ],
    }
)
