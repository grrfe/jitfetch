#!/usr/bin/python3.10
from __future__ import annotations

import argparse
import json
import os.path
import sys
import time
import webbrowser

import git
import semver
from git.refs.tag import TagReference

from jitfetch import has_version, trigger_version_build, request_build_log, get_web_url


def arg_or_default(args: argparse.Namespace, arg: str, default: str):
    if args is None:
        return default

    return vars(args)[arg] if args is None or arg in args else default


def read_config():
    with open("jitfetch.json", "r") as config_file:
        _json = json.load(config_file)
        if "repo" not in _json:
            print("Incorrect config!")
            exit(1)

        print("Running using jitpack.json")
        repo = _json["repo"]
        wait = _json["wait"] if "wait" in _json else 0
        notify = _json["notify"] if "notify" in _json else False

    return repo, wait, notify


def parse_tags():
    current_dir = os.getcwd()
    git_repo = git.Repo(current_dir)

    semver_tags = list(
        map(lambda t: semver.Version.parse(t.__str__()),
            filter(lambda t: isinstance(t, TagReference), git_repo.tags)
            )
    )

    tags = sorted(semver_tags)
    if len(tags) == 0:
        return None

    found_tags = ", ".join(map(lambda t: t.__str__(), tags))
    print(f"Found tags: {found_tags}")

    return tags[-1].__str__()


def add_fetch_arguments(_parser):
    _parser.add_argument("-w", "--wait", help="Delay in seconds before fetching", type=int, default=0,
                         action="store")
    _parser.add_argument("-n", "--notify", help="Send result as notification", action="store_true")


def create_command_parser(_parser, include_fetch: bool, required: bool):
    subparsers = _parser.add_subparsers(help="jitfetch commands", dest="command", required=required)
    subparsers.add_parser("list", help="Print available versions")

    if include_fetch:
        fetch_parser = subparsers.add_parser("fetch", help="Fetch a given version")
        fetch_parser.add_argument("version", help="Version to fetch")
        add_fetch_arguments(fetch_parser)

    log_parser = subparsers.add_parser("log", help="Display build log for a given version")
    log_parser.add_argument("version", help="Log of version")

    subparsers.add_parser("web", help="Open the Jitpack webpage of the provided repo")

    return subparsers


__COMMANDS_WITHOUT_FETCH = ["log", "list", "web"]


def parse_input():
    repo = None
    wait = 0
    notify = False
    command = "fetch"

    has_config = os.path.exists("jitfetch.json")
    if has_config:
        repo, wait, notify = read_config()

    latest_tag = None
    is_git_dir = os.path.isdir(".git")
    if has_config and not is_git_dir:
        print("jitfetch.json may only be present in git repositories!")
        exit(2)

    if is_git_dir:
        latest_tag = parse_tags()
        if latest_tag is None:
            print("No tags found")
            exit(1)

    parser = argparse.ArgumentParser(description="Fetch jitpack.io releases")
    args = None

    has_args = len(sys.argv) > 1
    if has_args:
        if not has_config:
            parser.add_argument("repo", help="Repo in the following format: com.gitlab.grrfe/jitfetch")

        create_command_parser(parser, not has_config, not has_config or sys.argv[1] in __COMMANDS_WITHOUT_FETCH)

        if has_config:
            add_fetch_arguments(parser)
            parser.set_defaults(command=command)

        args = parser.parse_args()

    repo = arg_or_default(args, "repo", repo)
    if repo is None:
        print("No repository provided!")
        exit(2)

    command = arg_or_default(args, "command", command)

    version = arg_or_default(args, "version", latest_tag)
    notify = arg_or_default(args, "notify", notify)
    wait = arg_or_default(args, "wait", wait)

    is_fetch = (command == "fetch")
    if is_fetch and version is None:
        print("No version provided!")
        exit(2)

    if is_fetch:
        print(f"{repo} @ {version}")
    else:
        print(repo)

    if is_fetch:
        if wait > 0:
            print(f"Waiting for {wait} seconds")
            time.sleep(wait)

        versions = has_version(repo)
        if versions is not None:
            trigger_version_build(repo, version, notify)
    elif command == "log":
        versions = has_version(repo)
        if versions is not None:
            print(request_build_log(repo, version))
    elif args.command == "list":
        versions = has_version(repo)
        if versions is not None:
            print(json.dumps(versions, indent=4))
    elif args.command == "web":
        webbrowser.open(get_web_url(repo))


if __name__ == "__main__":
    parse_input()
