from __future__ import annotations

import time
from typing import Union, Tuple

import requests
from notifypy import Notify

__JITPACK_BASE_URL = "https://jitpack.io"
__FETCH_URL = f"{__JITPACK_BASE_URL}/api/versions/{{0}}"
__BUILD_URL = f"{__JITPACK_BASE_URL}/api/builds/{{0}}/{{1}}"
__BUILT_REPO = f"{__JITPACK_BASE_URL}/{{0}}/{{1}}"
__BUILD_FILES = "Files:"


def get_web_url(repo: str) -> str:
    return f"{__JITPACK_BASE_URL}#{repo}"


def fetch_versions(repo_url: str):
    dep_version_req = requests.get(__FETCH_URL.format(repo_url))

    slash_idx = repo_url.index("/")
    git_user = repo_url[:slash_idx]
    repo_name = repo_url[slash_idx + 1:]

    if dep_version_req.status_code == 200:
        _json = dep_version_req.json()
        if git_user in _json and repo_name in _json[git_user]:
            return _json[git_user][repo_name]


def send_trigger_build_req(url) -> int:
    return requests.get(url).status_code


def check_build_status(url):
    return requests.get(url).json()


def trigger_version_build(repo: str, version_to_build: str, notify: bool = False) -> Union[bool | Tuple[str, str]]:
    url = __BUILD_URL.format(repo, version_to_build)
    status_code = send_trigger_build_req(url)

    if status_code == 200:
        print_and_notify(notify, "Version has already been built!")
        return True
    elif status_code == 404:
        print("Build has been triggered..")

        status = None
        message = None
        while True:
            build_status = check_build_status(url)

            status = build_status["status"]
            message = build_status["message"]
            if status.lower() != "none" or message != "":
                break

            time.sleep(30)

        if status == "ok":
            print_and_notify(notify, f"Build finished, status: {status}")
            return True
        else:
            print_and_notify(notify, f"Build failed, status: {status}, message: {message}")
            return status, message


def print_and_notify(notify: bool, message: str):
    if notify:
        notification = Notify()

        notification.title = "Build update"
        notification.message = message

        notification.send()

    print(message)


def request_build_log(repo: str, version: str) -> str:
    build_info = __BUILT_REPO.format(repo.replace(".", "/"), version)

    log = requests.get(f"{build_info}/build.log").text
    lines = []

    prepend_url = False
    for line in log.splitlines():
        line = line.strip()

        if len(line) > 0 and prepend_url:
            lines.append(f"{__JITPACK_BASE_URL}/{line}")
            continue

        if line == __BUILD_FILES:
            prepend_url = True

        lines.append(line)

    return "\n".join(lines)


def has_version(repo: str):
    versions = fetch_versions(repo)
    if versions is not None:
        return versions
    else:
        print("No repo found!")
        return None
