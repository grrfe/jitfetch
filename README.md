# JitFetch

CLI client for [JitPack](https://jitpack.io)

**Requirements**

* Python3 

**Installation**

* Clone the repo (`git clone https://gitlab.com/grrfe/jitfetch`)  
* Change dir (`cd jitfetch`)
* Install dependencies (`pip3 install -r requirements.txt`)
* Run (`python3 jitfetch.py`)

**Usage**

```  
grrfe@gitlab:~$ jitfetch --help
usage: jitfetch [-h] repo {list,fetch,log,web} ...

Fetch jitpack.io releases

positional arguments:
  repo                  Repo in the following format:
                        com.gitlab.grrfe/jitfetch
  {list,fetch,log,web}  jitfetch commands
    list                Print available versions
    fetch               Fetch a given version
    log                 Display build log for a given version
    web                 Open the Jitpack webpage of the provided repo

options:
  -h, --help            show this help message and exit
```